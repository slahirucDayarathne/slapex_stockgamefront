package Model;

import java.io.Serializable;
import java.util.List;

public class AllGAmeDataMessage implements Serializable{
    public List<Player1> topPlayer ;
    public List<PlayerGame> playerGame;
    public List<Double> evenDoubleList;

    public AllGAmeDataMessage(List l1 , List l2 ,List l3){
        topPlayer = l1;
        playerGame = l2;
        evenDoubleList = l3;
    }
    public AllGAmeDataMessage(){
        super();
    }
}
