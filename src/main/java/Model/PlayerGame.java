package Model;

public class PlayerGame {

    public double[] sharePrices;
    public String[] shareAmounts;

    public PlayerGame(double[] sharePrices , String[] shareAmounts){
        this.sharePrices = sharePrices;
        this.shareAmounts = shareAmounts;
    }
    public PlayerGame(){
        super();
    }
}
