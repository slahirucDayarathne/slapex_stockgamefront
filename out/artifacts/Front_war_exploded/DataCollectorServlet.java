package Controller;

import Model.PlayerGame;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "DataCollectorServlet", urlPatterns = {"/data"})
public class DataCollectorServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        out.println("Hello Here Came");
        String Dell = request.getParameter("Dell");
        String IBM = request.getParameter("IBM");
        String Pearson	= request.getParameter("Pearson");
        String McGrewHill = request.getParameter("McGrewHill");
        String Hilton = request.getParameter("Hilton");
        String Shangrilla = request.getParameter("Shangrilla");
        String JohnDeer = request.getParameter("JohnDeer");
        String Syscor = request.getParameter("Syscor");
        String BMW = request.getParameter("BMW");
        String AstonMartin = request.getParameter("Aston Martin");

        double dellPrice = 10.75;
        double ibmPrice = 18.45;
        double pearsonPrice = 7.50;
        double grewPrice = 3.20;
        double hiltonPrice =  	1.68;
        double shangrillaPrice =  	1.34;
        double JohnDeerPrice = 3.25;
        double SyscorPrice = 8.75;
        double BMWPrice = 1.34;
        double astonPrice = 1.85;

        String[] stockString = {Dell, IBM, Pearson, McGrewHill, Hilton , Shangrilla , JohnDeer , Syscor , BMW , AstonMartin};

        for (int a = 0 ; a < stockString.length ; a++){
            out.println(stockString[a]);
        }

        double[] stockPrice = {dellPrice, ibmPrice, pearsonPrice, grewPrice,hiltonPrice,shangrillaPrice,JohnDeerPrice, SyscorPrice,BMWPrice,astonPrice};

        for (int a = 0 ; a < stockPrice.length ; a++){
            out.println(stockPrice[a]);
        }

        PlayerGame game = new PlayerGame(stockPrice,stockString);
        ObjectMapper obj = new ObjectMapper();
        String s = obj.writeValueAsString(game);


        CloseableHttpClient client = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost("http://localhost:8087/player/lahiru");
        StringEntity entity = new StringEntity(s);
        httpPost.setEntity(entity);
        httpPost.setHeader("Accept", "application/json");
        httpPost.setHeader("Content-type", "application/json");

        CloseableHttpResponse response1 = client.execute(httpPost);


        out.println(s);
        response.sendRedirect("./MainPlay.jsp");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
