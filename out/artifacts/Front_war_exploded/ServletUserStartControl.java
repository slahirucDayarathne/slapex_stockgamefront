package Controller;

import Model.Player;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import javax.servlet.GenericServlet;
import java.io.IOException;
import java.io.PrintWriter;

@javax.servlet.annotation.WebServlet(name = "ServletUserStartControl" , urlPatterns = {"/hello"})
public class ServletUserStartControl extends javax.servlet.http.HttpServlet {
    protected void doPost(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {

        String playerName = request.getParameter("playerName");
        String playerBalance = "1000";
        Player player = new Player(playerName , playerBalance);

//        Gson g = new Gson();
        String s ;

        ObjectMapper obj = new ObjectMapper();
        s = obj.writeValueAsString(player);
        PrintWriter out = response.getWriter();
        out.println(s);
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        out.println("Lahiru Chandima");
//        out.println(ss);
        //response.sendRedirect("./MainMenu.jsp");
        try {
            Thread.sleep(3000);
            response.sendRedirect("./MainMenu.jsp");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        out.flush();


        CloseableHttpClient client = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost("http://localhost:8087/player");
        StringEntity entity = new StringEntity(s);
        httpPost.setEntity(entity);
        httpPost.setHeader("Accept", "application/json");
        httpPost.setHeader("Content-type", "application/json");

        CloseableHttpResponse response1 = client.execute(httpPost);




    }

    protected void doGet(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {

    }
}
