<%@ page import="Model.Player" %>
<%@ page import="java.io.PrintWriter" %><%--
  Created by IntelliJ IDEA.
  User: Lahiru Chandima
  Date: 07/08/2019
  Time: 22:21
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css">

    <style>
        body { background-image:url('stock-market-background_2130593.png'); background-repeat:no-repeat;
            background-size: 100%  }
        tr{height: 20px;}
        td,th{width: 150px;
            background-color: transparent;}
    </style>
</head>



<body>

<form action="./data" method="post">

    <div class="container-fluid">

        <div class="row">
            <div class="col-md-12">

                <div class="col-md-8" style="background-color: black; padding: 20px;border-radius: 25px;
  border: 2px solid #73AD21;color: black; width: 800px;height: 400px;">


                    <table class="table table-bordered" style="color: white; font-size: 20px; margin: 5px;padding: 10px;margin: 5px;">
                        <thead>
                        <tr>

                            <th>
                                Stock sector
                            </th>
                            <th>
                                Company
                            </th>
                            <th>
                                Share amount
                            </th>
                            <th>
                                Share price
                            </th>
                            <th>
                                Selecting Amount
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>Technology</td>
                            <td>Dell</td>
                            <td>200</td>
                            <td>10.75</td>
                            <td><input type="text" value="0" name="Dell"></td>
                        </tr>
                        <tr class="table-active">
                            <td>Technology</td>
                            <td>IBM</td>
                            <td>100</td>
                            <td>18.45</td>
                            <td><input type="text" value="0" name="IBM"></td>
                        </tr>
                        <tr class="table-success">
                            <td>Education</td>
                            <td>Pearson</td>
                            <td>200</td>
                            <td>7.50</td>
                            <td><input type="text" value="0" name="Pearson"></td>
                        </tr>
                        <tr class="table-warning">
                            <td>Education</td>
                            <td>Mc Grew Hill</td>
                            <td>200</td>
                            <td>3.20</td>
                            <td><input type="text" value="0" name="McGrewHill"></td>
                        </tr>
                        <tr class="table-danger">
                            <td>Tourism</td>
                            <td>Hilton</td>
                            <td>100</td>
                            <td>1.68</td>
                            <td><input type="text" value="0" name="Hilton"></td>
                        </tr>
                        <tr class="table-danger">
                            <td>Tourism</td>
                            <td>Shangrilla</td>
                            <td>100</td>
                            <td>1.34</td>
                            <td><input type="text" value="0" name="Shangrilla"></td>
                        </tr>
                        <tr class="table-warning">
                            <td>Agriculture</td>
                            <td>John Deer</td>
                            <td>200</td>
                            <td>3.25</td>
                            <td><input type="text" value="0" name="JohnDeer"></td>
                        </tr>
                        <tr class="table-warning">
                            <td>Agriculture</td>
                            <td>Syscor</td>
                            <td>200</td>
                            <td name="Mypara" value="800">8.75</td>
                            <td><input type="text" value="0" name="Syscor"></td>
                        </tr>
                        <tr>
                            <td>Transport</td>
                            <td>BMW</td>
                            <td>200</td>
                            <td>1.34</td>
                            <td><input type="text" value="0" name="BMW"></td>
                        </tr>
                        <tr>
                            <td>Transport</td>
                            <td>Aston Martin</td>
                            <td>200</td>
                            <td>1.85</td>
                            <td><input type="text" value="0" name="Aston Martin"></td>
                        </tr>
                        </tbody>
                    </table>

                </div>
                <div class="col-md-4" style="background-image: url('s7ln3_.gif');background-repeat: no-repeat;padding: 20px;border-radius: 25px;
  border: 2px solid #73AD21; width: 275px;height: 130px; float: right;margin-right: 100px;"></div>
            </div>
        </div>
    </div>
    <br>
    <input class="btn btn-primary" type="submit" value="Submit" style="background-color:#70d2db;height: 40px;width: 70px;border-radius: 5px; color: black; margin-left: 75px;">
</form>




<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

</body>
</html>
