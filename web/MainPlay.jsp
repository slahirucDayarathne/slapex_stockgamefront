<%--
  Created by IntelliJ IDEA.
  User: Lahiru Chandima
  Date: 08/08/2019
  Time: 12:48
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.util.Random"%>
<%@ page import="Model.PlayerGame" %>
<%@ page import="com.fasterxml.jackson.databind.ObjectMapper" %>
<%@ page import="org.apache.http.impl.client.CloseableHttpClient" %>
<%@ page import="org.apache.http.impl.client.HttpClients" %>
<%@ page import="org.apache.http.client.methods.HttpPost" %>
<%@ page import="org.apache.http.entity.StringEntity" %>
<%@ page import="org.apache.http.client.methods.CloseableHttpResponse" %>
<%@ page import="org.apache.http.client.methods.HttpGet" %>
<%@ page import="org.apache.http.HttpResponse" %>
<%@ page import="org.apache.http.HttpEntity" %>
<%@ page import="java.io.InputStream" %>
<%@ page import="org.apache.commons.io.IOUtils" %>
<%@ page import="Model.AllGAmeDataMessage" %>
<%@ page import="Model.Broker" %>
<%@ page import="java.util.ArrayList" %>
<html>
<head>
    <title>Stock Game</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body style="background-image: url('stock-market-background_2130593.png'); color: black;">

<h1 style="color: white;">SLApex Stock Broker Game</h1>

<h3 style="color: white;"> Wait and respond. Amazing stock events going to happen...</h3>
<%
    CloseableHttpClient httpClient = HttpClients.createDefault();
    HttpGet getRequest= new HttpGet("http://localhost:8087/player/lahiru");
    getRequest.addHeader("accept", "application/json");
    HttpResponse newresponse = httpClient.execute(getRequest);
    HttpEntity entity = newresponse.getEntity();
    InputStream is = entity.getContent();
    String s = IOUtils.toString(is);
//    out.println(s);
    ObjectMapper mapper = new ObjectMapper();
    AllGAmeDataMessage message = mapper.readValue(s,AllGAmeDataMessage.class);

//    out.println(message.topPlayer.get(0).name);
//    out.println(message.topPlayer.get(1).name);

    double sum = 0;
    double midValue;

    if(Broker.a == 0){
        for (int a = 0 ; a < message.playerGame.get(0).sharePrices.length ; a++){
            Broker.sharepriceList1.add(message.playerGame.get(0).sharePrices[a]+(1.0+ message.evenDoubleList.get(0)/100.00));
        }
        for (int a = 0 ; a < message.playerGame.get(0).sharePrices.length ; a++){
            Broker.sharepriceList2.add(message.playerGame.get(0).sharePrices[a]+(1.0+ message.evenDoubleList.get(1)/100.00));
        }
        for (int a = 0 ; a < message.playerGame.get(0).sharePrices.length ; a++){
            Broker.sharepriceList3.add(message.playerGame.get(0).sharePrices[a]+(1.0+ message.evenDoubleList.get(2)/100.00));
        }
        for (int a = 0 ; a < message.playerGame.get(0).sharePrices.length ; a++){
            Broker.sharepriceList4.add(message.playerGame.get(0).sharePrices[a]+(1.0+ message.evenDoubleList.get(3)/100.00));
        }
        for (int a = 0 ; a < message.playerGame.get(0).sharePrices.length ; a++){
            Broker.sharepriceList5.add(message.playerGame.get(0).sharePrices[a]+(1.0+ message.evenDoubleList.get(4)/100.00));
        }
    }

    for(int a = 0 ; a  < message.playerGame.get(0).sharePrices.length ; a++ ){

        if(Broker.a == 1 || Broker.a == 2){
            midValue = (message.playerGame.get(0).sharePrices[a] * (1.0 + message.evenDoubleList.get(0)))* Double.parseDouble(message.playerGame.get(0).shareAmounts[a]);
            sum = sum + midValue;}
        if(Broker.a == 3 || Broker.a == 4){
            midValue = (message.playerGame.get(0).sharePrices[a] * (1.0 +message.evenDoubleList.get(1)))* Double.parseDouble(message.playerGame.get(0).shareAmounts[a]);
            sum = sum + midValue;}
        if(Broker.a == 5 || Broker.a == 6){
            midValue = (message.playerGame.get(0).sharePrices[a] * (1.0 +message.evenDoubleList.get(2)))* Double.parseDouble(message.playerGame.get(0).shareAmounts[a]);
            sum = sum + midValue;}
        if(Broker.a == 7 || Broker.a == 8){
            midValue = (message.playerGame.get(0).sharePrices[a] * (1.0 +message.evenDoubleList.get(3)))* Double.parseDouble(message.playerGame.get(0).shareAmounts[a]);
            sum = sum + midValue;}
        if(Broker.a == 9 || Broker.a == 10){
            midValue = (message.playerGame.get(0).sharePrices[a] * (1.0 +message.evenDoubleList.get(4)))* Double.parseDouble(message.playerGame.get(0).shareAmounts[a]);
            sum = sum + midValue;}
//        else{
//            midValue = (message.playerGame.get(0).sharePrices[a] * (1.0 +message.evenDoubleList.get(4)))* Double.parseDouble(message.playerGame.get(0).shareAmounts[a]);
//            sum = sum + midValue;
//        }
        Broker.p1 += sum;
    }

    double sum2 = 0.0 ;
    for(int a = 0 ; a  < message.playerGame.get(1).sharePrices.length ; a++ ){
        midValue = 0 ;
        if(Broker.a == 1 || Broker.a == 2){
            midValue = (message.playerGame.get(1).sharePrices[a] * (1.0 +message.evenDoubleList.get(0)))* Double.parseDouble(message.playerGame.get(1).shareAmounts[a]);
            sum2 = sum2 + midValue;}
        if(Broker.a == 3 || Broker.a == 4){
            midValue = (message.playerGame.get(1).sharePrices[a] * (1.0 +message.evenDoubleList.get(1)))* Double.parseDouble(message.playerGame.get(1).shareAmounts[a]);
            sum2 = sum2 + midValue;}
        if(Broker.a == 5|| Broker.a == 6){
            midValue = (message.playerGame.get(1).sharePrices[a] * (1.0 +message.evenDoubleList.get(2)))* Double.parseDouble(message.playerGame.get(1).shareAmounts[a]);
            sum2 = sum2 + midValue;}
        if(Broker.a == 7 || Broker.a == 8){
            midValue = (message.playerGame.get(1).sharePrices[a] * (1.0 +message.evenDoubleList.get(3)))* Double.parseDouble(message.playerGame.get(1).shareAmounts[a]);
            sum2 = sum2 + midValue;}
        if(Broker.a == 9 || Broker.a == 10){
            midValue = (message.playerGame.get(1).sharePrices[a] * (1.0 +message.evenDoubleList.get(4)))* Double.parseDouble(message.playerGame.get(1).shareAmounts[a]);
            sum2 = sum2 + midValue;}
//        else{
//            midValue = (message.playerGame.get(1).sharePrices[a] * (1.0 +message.evenDoubleList.get(4)))* Double.parseDouble(message.playerGame.get(1).shareAmounts[a]);
//            sum2 = sum2 + midValue;
//        }
        Broker.p2 += sum2;
    }
%>



<script>
    var loadValue = window.sessionStorage.getItem("pageLoad");
    loadValue++;
    window.sessionStorage.setItem("pageLoad" , loadValue);
    window.alert("You are in the game ROUND : :" + loadValue)
</script>

<%--<h1>Hello There</h1>--%>

<div class="container-fluid">

    <div class="row">
        <div class="col-sm-6" style="background-image: url('table1.jpg');color: white;">
            <div>
                <h3>Player Name : <%= message.topPlayer.get(0).name %></h3>
            </div>
            <div>
                <center>
                <table border="1" style="padding: 5px;text-align: center;color: white;">
                    <tr>
                        <th>Company name</th>
                        <th>Share Price</th>
                        <th>Share Amount</th>
                    </tr>
                    <tr>
                        <td>Dell</td>
                        <td><%= Math.round(message.playerGame.get(0).sharePrices[0] * (1.0+message.evenDoubleList.get(0) )*100)/100.00%></td>
                        <td><%= message.playerGame.get(0).shareAmounts[0] %></td>
                    </tr>
                    <tr>
                        <td>IBM</td>
                        <td><%= Math.round(message.playerGame.get(0).sharePrices[1] * (1.0+message.evenDoubleList.get(0))*100)/100.00%></td>
                        <td><%= message.playerGame.get(0).shareAmounts[1] %></td>
                    </tr>
                    <tr>
                        <td>Pearson</td>
                        <td><%= Math.round(message.playerGame.get(0).sharePrices[2] * (1.0+message.evenDoubleList.get(1))*100)/100.00%></td>
                        <td><%= message.playerGame.get(0).shareAmounts[2] %></td>
                    </tr>
                    <tr>
                        <td>Mc Grew Hill</td>
                        <td><%= Math.round(message.playerGame.get(0).sharePrices[3] * (1.0+message.evenDoubleList.get(1))*100)/100.00%></td>
                        <td><%= message.playerGame.get(0).shareAmounts[3] %></td>
                    </tr>
                    <tr>
                        <td>Hilton</td>
                        <td><%= Math.round(message.playerGame.get(0).sharePrices[4] * (1.0+message.evenDoubleList.get(2))*100)/100.00%></td>
                        <td><%= message.playerGame.get(0).shareAmounts[4] %></td>
                    </tr>
                    <tr>
                        <td>Shangrilla</td>
                        <td><%= Math.round(message.playerGame.get(0).sharePrices[5] * (1.0+message.evenDoubleList.get(2))*100)/100.00%></td>
                        <td><%= message.playerGame.get(0).shareAmounts[5] %></td>
                    </tr>
                    <tr>
                        <td>John Deer</td>
                        <td><%= Math.round(message.playerGame.get(0).sharePrices[6] * (1.0+message.evenDoubleList.get(3))*100)/100.00%></td>
                        <td><%= message.playerGame.get(0).shareAmounts[6] %></td>
                    </tr>
                    <tr>
                        <td>Syscor</td>
                        <td><%= Math.round(message.playerGame.get(0).sharePrices[7] * (1.0+message.evenDoubleList.get(3))*100)/100.00%></td>
                        <td><%= message.playerGame.get(0).shareAmounts[7] %></td>
                    </tr>
                    <tr>
                        <td>BMW</td>
                        <td><%= Math.round(message.playerGame.get(0).sharePrices[8] * (1.0+message.evenDoubleList.get(4))*100)/100.00%></td>
                        <td><%= message.playerGame.get(0).shareAmounts[8] %></td>
                    </tr>
                    <tr>
                        <td>Aston Martin</td>
                        <td><%= Math.round(message.playerGame.get(0).sharePrices[9] * (1.0+message.evenDoubleList.get(4))*100)/100.00%></td>
                        <td><%= message.playerGame.get(0).shareAmounts[9] %></td>
                    </tr>
                    <tr>
                        <td>Total</td>
                        <td>
                            <%=sum %>
                        </td>
                        <td>------</td>
                    </tr>
                </table>
                </center>
                <h3>Player Total : <%= Math.round(Broker.p1*100)/100.00%></h3>
            </div>

        </div>
        <div class="col-sm-6" style="background-image: url('table2.jpg');color: white;">
            <div>
                <h3>Player Name : <%= message.topPlayer.get(1).name %></h3>
            </div>
            <div>
                <center>
                <table border="1" style="padding: 5px;text-align: center;color: white;">
                <tr>
                    <th>Company name</th>
                    <th>Share Price</th>
                    <th>Share Amount</th>
                </tr>
                <tr>
                    <td>Dell</td>
                    <td><%= Math.round(message.playerGame.get(1).sharePrices[1] * (1.0+message.evenDoubleList.get(0))*100)/100.00%></td>
                    <td><%= message.playerGame.get(1).shareAmounts[1] %></td>
                </tr>
                <tr>
                    <td>IBM</td>
                    <td><%= Math.round(message.playerGame.get(1).sharePrices[1] * (1.0+message.evenDoubleList.get(0))*100)/100.00%></td>
                    <td><%= message.playerGame.get(1).shareAmounts[1] %></td>
                </tr>
                <tr>
                    <td>Pearson</td>
                    <td><%= Math.round(message.playerGame.get(1).sharePrices[2] * (1.0+message.evenDoubleList.get(1))*100)/100.00%></td>
                    <td><%= message.playerGame.get(1).shareAmounts[2] %></td>
                </tr>
                <tr>
                    <td>Mc Grew Hill</td>
                    <td><%= Math.round(message.playerGame.get(1).sharePrices[3] * (1.0+message.evenDoubleList.get(1))*100)/100.00%></td>
                    <td><%= message.playerGame.get(1).shareAmounts[3] %></td>
                </tr>
                <tr>
                    <td>Hilton</td>
                    <td><%= Math.round(message.playerGame.get(1).sharePrices[4] * (1.0+message.evenDoubleList.get(2))*100)/100.00%></td>
                    <td><%= message.playerGame.get(1).shareAmounts[4] %></td>
                </tr>
                <tr>
                    <td>Shangrilla</td>
                    <td><%= Math.round(message.playerGame.get(1).sharePrices[5] * (1.0+message.evenDoubleList.get(2))*100)/100.00%></td>
                    <td><%= message.playerGame.get(1).shareAmounts[5] %></td>
                </tr>
                <tr>
                    <td>John Deer</td>
                    <td><%= Math.round(message.playerGame.get(1).sharePrices[6] * (1.0+message.evenDoubleList.get(3))*100)/100.00%></td>
                    <td><%= message.playerGame.get(1).shareAmounts[6] %></td>
                </tr>
                <tr>
                    <td>Syscor</td>
                    <td><%= Math.round(message.playerGame.get(1).sharePrices[7] * (1.0+message.evenDoubleList.get(3))*100)/100.00%></td>
                    <td><%= message.playerGame.get(1).shareAmounts[7] %></td>
                </tr>
                <tr>
                    <td>BMW</td>
                    <td><%= Math.round(message.playerGame.get(1).sharePrices[8] * (1.0+message.evenDoubleList.get(4))*100)/100.00%></td>
                    <td><%= message.playerGame.get(1).shareAmounts[8] %></td>
                </tr>
                <tr>
                    <td>Aston Martin</td>
                    <td><%= Math.round(message.playerGame.get(1).sharePrices[9] * (1.0+message.evenDoubleList.get(4))*100)/100.00%></td>
                    <td><%= message.playerGame.get(1).shareAmounts[9] %></td>
                </tr>
                <tr>
                    <td>Total</td>
                    <td><%= sum2 %></td>
                    <td>-----</td>
                </tr>
            </table>
                    </center>
            <h3>Player Total : <%= Math.round(Broker.p2*100)/100.00%></h3>
        </div>
    </div>
</div>

<%
    Broker.a++;
//    out.println(Broker.a);
//    out.println("AAA:" + Broker.p1);
//    System.out.println(Broker.a);
//    System.out.println("P1" + Broker.p1);
//    System.out.println("P2**" +Broker.p2);
%>

<%--#######################################################--%>

<script>

    var count = <%= Broker.a%> ;
    var time = new Date().getTime();



    function refresh() {
        if( loadValue < 5){
            count++;
            if(loadValue == 1){window.alert("New Budget Release !!!");}
            if(loadValue == 2){window.alert("Election Announced !!!");}
            if(loadValue == 3){window.alert("Bomb Blast Happened !!!");}
            if(loadValue == 4){window.alert("Government TAX Release Announced");}
            if(loadValue == 5){window.alert("New Cabinet Appointed !!!");}

            window.location.reload(true);
        }
        else
            setTimeout(refresh, 10000);

    }

    setTimeout(refresh, 10000);
</script>


<%--####################################################--%>
<%
    if(Broker.a > 5){
        String player1Name = message.topPlayer.get(0).name;;
        String player2Name = message.topPlayer.get(1).name;
        String winnerName = "a";
            if(sum2 > sum)
                winnerName = player2Name;
            else
                winnerName = player1Name;
        %>
        <script>

            if(loadValue == 5){
            var p = "<%= winnerName%>";
            window.alert("Congratulations winner is : " + p);
            }
        </script>
<%
    }
%>

<%--############################333333333333333333333333333333333--%>

</body>

</html>
